// Lab Exercise 3 Mad Lib
// James Darling

#include <iostream>
#include <conio.h>
#include <string>
#include <stdio.h>
#include <fstream>
#include <sstream>


using namespace std;
void instructions();
void getwords();


int main()
{
	

	
	char choice;
	instructions();
	getwords();
	
	cout << endl;
	
	_getch();
	return 0;
	
}
void instructions()
{
	cout << "Welcome to a game a Mad libs!  You will begin by typing text to answer each prompt." << endl;
	cout << "Afterwards the answers you chose will be put into a mad lib paragraph." << endl;
	cout << "At the end of the program you will have the option to save the output to a file." << endl;
	cout << endl;
}


void getwords()
{
	const int Size = 16;
	 string words[Size] = { " Plural Noun: ", "n adjective: ", "n animal: ", " plural noun: ", \
		"n adjective: ", " color: ", "n adjective: ", " noun: ", " plural noun: ", "n adjective: ", \
		" verb: ", " plural noun: ", " verb (past tense): ", " verb: ", " noun: ", "n adjective: " };

	string input[Size];
	
		for (int i = 0; i < Size; i++)
		{
			cout << "Enter a" << words[i]; getline(cin, input[i]);
		}
		cout << endl;
		
		
		cout << "Unicorns aren't like other " << input[0] << ", they're " << input[1] << ". " <<
			" They look like a(n) " << input[2] << ", with " << input[3] << " for feet and a " << input[4] <<
			" mane of hair.  But Unicorns are " << input[5] << " and have a " << input[6] << " " << input[7] <<
			" on their heads. Some " << input[8] << " don't believe unicorns are " << input[9] <<
			", but I believe in them. I would love to " << input[10] << " a unicorn to faraway " << input[11] <<
			".  One thing I've always " << input[12] << " about is whether unicorns " << input[13] << " rainbows, or is their "
			<< input[14] << " " << input[15] << " like any other animal's?" << endl;
		
		string output = "Unicorns aren't like other " + input[0] + ", they're " + input[1] + ". " +
			" They look like a(n) " + input[2] + ", with " + input[3] + " for feet and a " + input[4] +
			" mane of hair.  But Unicorns are " + input[5] + " and have a " + input[6] + " " + input[7] +
			" on their heads. Some " + input[8] + " don't believe unicorns are " + input[9] +
			", but I believe in them. I would love to " + input[10] + " a unicorn to faraway " + input[11] +
			".  One thing I've always " + input[12] + " about is whether unicorns " + input[13] + " rainbows, or is their "
			+ input[14] + " " + input[15] + " like any other animal's?";
		
		cout << endl;
		char choice;
		cout << "Would you like to save output to file?  (y/n): ";
		cin >> choice;
		if (choice == 'y')
		{
			string path = "C:\\C++\\madlib.txt";
			ofstream ofs(path);
			ofs << output;
			ofs.close();
			cout << "Mad lib has been saved to madlib.txt" << endl;
			cout << "Press any key to exit! ";

		}
		else if (choice == 'n')
		{
			cout << "Thank you for playing!" << endl;
			cout << "Press any key to exit! ";
			
		}
		
		
	
		
}



/* Unicorns aren't like other ( plural noun); they're (adjective).
They look like (an animial), with (plural noun) for feet and a (adjective)
mane of hair.  But Unicorns are (color) and have a (adjective)(noun) on their
heads.  Some (plural noun) don't believe unicorns are (adjective), but I believe
in them.  I would love to (verb) a unicorn to faraway (plural noun).  One thing
I've always (verb -ed) about is whether unicorns (verb) rainbows, or is  their
(noun)(adjective) like any other animal's? */
